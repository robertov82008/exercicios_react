import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';
import registerServiceWorker from './registerServiceWorker';

import Fields from './ex/fields';

const reducers = combineReducers({
    field: () => ({ value: '' })
});

ReactDOM.render(
    <Provider store={createStore(reducers)}>
        <Fields initialValue='' />
    </Provider>
    , document.getElementById('root'));

registerServiceWorker();
